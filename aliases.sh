# Aliases

## Variables de ambiente

export EDITOR=vim
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/.cargo/bin:$PATH
export VENV=.venv
export LENGTH=79
export FONT_DIR=$HOME/.fonts
export BASH_ALIAS=$HOME/.bash_aliases
export REPO_ALIAS=https://gitlab.com/perritotuerto/codigo/aliases/-/raw/no-masters/aliases.sh
export FLINSTALL=/usr
export FREELINGDIR=$FLINSTALL
if [ -f $HOME/Repositorios/aliases/aliases.sh ] || [ -f $HOME/Repositories/aliases/aliases.sh ]; then
  export RLOC_ALIAS=($HOME/Repositori*s/aliases/aliases.sh)
else
  export RLOC_ALIAS=/foo
fi

## Eliminación de alias previos

unalias -a

## Atajos

alias .1='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias e='v'
alias g='geany'
alias l='lsd -al --permission octal'
alias o='okular'
alias s='scribus'
alias t='tree'
alias cpd='docker cp'
alias cdr='cd ~/Repositori*'
alias docs='cd ~/Document*'
alias repos='cdr'
alias cloud='nube'
alias descargas='cdd'
alias documentos='docs'
alias permisos='stat -c "Permiso %a | Creación %w | Modificación %y | %n" *'
alias stats='zsh_stats'

## Personalizaciones

alias mv='mv -v'
alias rm='rm -v'
alias cp='cp -rv'
alias df='df -hT'
alias du='du -sh'
alias sed='sed -zr'
alias ssh='ssh -o ServerAliveInterval=30'
alias bat='bat --paging never'
alias batcat='batcat --paging never'
alias free='free -h'
alias diff='diff --color=auto'
alias mkdir='mkdir -p'
alias rsync='rsync -varuchz --progress'
alias rdiff-backup='rdiff-backup -v 5'
alias perl-rename='perl-rename -v'
alias pip='pip3'
alias python='python3'
alias ipython='ipython3'
alias selene="selene --config $HOME/.config/selene/config.toml"
alias stylua="stylua --config-path $HOME/.config/stylua/config.toml"

## Acciones

alias buscar="grep -REin --color=auto --exclude-dir={.git,.venv,.pytest_cache,__pycache__,\*egg-info} --exclude=\*.min\*"
alias search="buscar"
alias pdfbuscar="pdfgrep --color=auto --page-number=label -PZRe 2>/dev/null"
alias pdfsearch="pdfbuscar"
alias rastrear='find . -iname'
alias trace='rastrear'
alias rastrear-todo='find / -iname'
alias trace-all='rastrear-todo'
alias copiar='xclip -selection clipboard <'
alias copy='copiar'
alias refresh='recargar'
alias install='instalar'
alias uninstall='desinstalar'
alias remove='desinstalar'
alias update='actualizar'
alias upgrade='actualizar'
alias enlistar='find . -type f'
alias list='enlistar'
alias comparar='compare'

## A modo de programitas

alias alv='rm -rf'
alias doc2md='pandoc --markdown-headings=atx -V lang=es'
alias mi-ip='curl -4 ifconfig.me -w "\n" && curl -6 ifconfig.me -w "\n"'
alias my-ip='mi-ip'
alias pecas-legacy="sudo docker start pecas && sudo docker exec -it pecas /bin/zsh -l"
alias pacman-limpiar='echo "Limpiando caché…" && sudo pacman -Scc && echo "\nEliminando paquetes huérfanos…" && sudo pacman -Rs $(pacman -Qqdt)'
alias pacman-clean='pacman-limpiar'
alias docker-limpiar='sudo docker system prune -a'
alias docker-clean='docker-limpiar'
alias añadir-fuentes='font-add'
alias tiempo='/usr/bin/t'
alias untar='tar -xvf'
alias untargz='tar -xvzf'
alias timestamp='date +%s'

## Monerías

alias arcoiris='journalctl --no-pager | dotacat'
alias matrix='tmatrix --mode=dense -f 0,5 -C magenta'
alias acuario='asciiquarium'
alias tren='sl'
alias gato='oneko &'

## Funciones

### Detecta Vim

v () {
  homebrew_vim=$(command -v /opt/homebrew/bin/vim >/dev/null 2>&1)
  if [[ $homebrew_vim ]]; then
    /opt/homebrew/bin/vim $@
  else
    vim $@
  fi
  unset homebrew_vim
}

### Detecta tipo de distribución

distro-name () {
  echo $(awk -F= '/^NAME/{print $2}' /etc/os-release) | sed 's/"//g'
}

### Imprime errores

echo-error () {
  echo "ERROR: $1"
  return 1
}

### Actualizador

actualizar () {
  case $1 in
  alias | aliases)
    echo "Actualizando alias"
    aliases actualizar
    ;;
  paquetes | packages)
    echo "Actualizando paquetes"
    if [[ $(distro-name) = "Arch Linux" ]]; then
      sudo pacman -Syy
      sudo pacman -S archlinux-keyring
    else
      sudo apt update
    fi
    ;;
  todo | sistema | all | system)
    echo "Actualizando sistema"
    if [[ $(distro-name) = "Arch Linux" ]]; then
      yay -Su
    else
      sudo apt upgrade
    fi
    ;;
  -h | --help | help | ayuda)
    actualizar-help ;;
  *)
    actualizar alias
    actualizar paquetes
    actualizar sistema
    ;;
  esac
}

actualizar-help () {
  echo "actualizar: utilidad para hacer actualizaciones.

Uso:
  actualizar [OPT]

Opciones:
  alias | aliases                Actualiza alias.
  paquetes | packages            Actualiza repositorio de paquetes.
  -h | --help | help | ayuda     Despliega esta ayuda.
  todo | sistema | all | system  Actualiza el sistema.
  vacío                          Actualiza el sistema con actualización previa de paquetes.

Ejemplos:
  actualizar alias
  actualizar paquetes
  actualizar -h
  actualizar todo
  actualizar"
}

### Instalador

instalar () {
  if [[ $(distro-name) = "Arch Linux" ]]; then
    yay -Syy $@
  else
    sudo apt install $@
  fi
}

### Desinstalador

desinstalar () {
  if [[ $(distro-name) = "Arch Linux" ]]; then
    yay -Rss $@
  else
    sudo apt purge $@
    sudo apt clean
    sudo apt autoremove
  fi
}

### Gestión del home

home () {
  if [ -d "$HOME/.git" ]; then
    case $1 in
    estado | status)
      git -C $HOME status
      ;;
    editar | edit)
      e $HOME/.gitignore
      ;;
    guardar | save)
      git -C $HOME add .
      git -C $HOME commit -m "Actualización"
      ;;
    empujar | push)
      if output=$(git -C $HOME status --porcelain) && [ ! -z "$output" ]; then
        home guardar
      fi
      unset output
      git -C $HOME pushall
      ;;
    -h | --help | help | ayuda)
      home-help ;;
    *)
      if [ -z "$1" ]; then
        echo-error "al menos un argumento es necesario; por ejemplo 'home -h'"
      else
        echo-error "comando '$1' inválido"
      fi
      ;;
    esac
  else
    echo-error "el home '$HOME' no es un directorio git"
  fi
}

home-help () {
  echo "home: utilidad para gestionar el home.

Home: $HOME
Estado: $(if [ -d "$HOME/.git" ]; then echo "OK"; else echo "requiere ser un repositorio git"; fi)

Uso:
  home [OPT]

Opciones:
  estado | status             Muestra el estado del home.
  editar | edit               Edita el archivo .gitignore del home.
  guardar | save              Guarda el estado del home.
  empujar | push              Empuja el estado del home al repositorio.
  -h | --help | help | ayuda  Despliega esta ayuda.

Ejemplos:
  home estado
  home editar
  home guardar
  home empujar
  home -h"
}

### Gestión de alias

aliases () {
  case $1 in
  actualizar | update)
    if [ -f "$RLOC_ALIAS" ]; then
      aliases link
    else
      curl -o $BASH_ALIAS $REPO_ALIAS
    fi
    recargar
    ;;
  ver | see)
    echo $BASH_ALIAS
    c $BASH_ALIAS
    ;;
  editar | edit)
    e $BASH_ALIAS
    ;;
  empujar | push)
    repo=$(parent $RLOC_ALIAS)
    if [ -f "$RLOC_ALIAS" ]; then
      if [ -f "$BASH_ALIAS" ] && [ ! -L "$BASH_ALIAS" ]; then
        cp -u $BASH_ALIAS $RLOC_ALIAS
      fi
      if output=$(git -C $repo status --porcelain) && [ ! -z "$output" ]; then
        git -C $repo add .
        git -C $repo commit -m "Actualización"
      fi
      git -C $repo pushall
      unset output repo
      recargar
    else
      echo-error "no se pudo empujar al repositorio; no existe el archivo '$RLOC_ALIAS'"
    fi
    ;;
  enlazar | link)
    if [ -f "$RLOC_ALIAS" ]; then
      if [ -f "$BASH_ALIAS" ]; then
        ln -fs $RLOC_ALIAS $BASH_ALIAS
      fi
      recargar
    else
      echo-error "no se encontró archivo '$RLOC_ALIAS' para copia."
    fi
    ;;
  -h | --help | help | ayuda)
    aliases-help ;;
  *)
    alias
    ;;
  esac
}

aliases-help () {
  echo "aliases: utilidad para hacer gestionar alias.

Uso:
  aliases [OPT]

Opciones:
  actualizar | update         Actualiza aliases.
  ver | see                   Muestra contenido de archivo de aliases.
  editar | edit               Edita aliases.
  empujar | push              Empuja los cambios locales al repositorio.
  enlazar | link              Genera enlace simbólico entre repo 'aliases' y archivo de aliases.
  -h | --help | help | ayuda  Despliega esta ayuda.
  vacío                       Uso regular de 'alias'.

Ejemplos:
  aliases ver
  aliases editar
  aliases -h
  aliases"
}

### Recarga aliases

recargar () {
  if [ -f "$HOME/.zshrc" ]; then
    source $HOME/.zshrc
  else
    source $HOME/.bashrc
  fi
}

### Respalda discos

bak () {
  echo -n "¿Respaldar $1 en $2? [y/N] "
  read REPLY
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    sudo dd if=$1 of=$2 bs=64K conv=noerror,sync status=progress
  else
    echo "No se realizó respaldo"
  fi
}

### Descarga información de sitios web

dump () {
  dump_domain=${@: -1}
  if [ "$1" = "mirror" ] || [ "$1" = "--mirror" ]; then
    wget --no-clobber --timestamping --continue --page-requisites --html-extension --convert-links \
    --mirror $dump_domain
  else
    wget --no-clobber --timestamping --continue --page-requisites --html-extension --convert-links \
    --recursive --follow-tags=a --domains=$dump_domain \
    --reject '*.js,*.css,*.ico,*.txt,*.gif,*.jpg,*.jpeg,*.png,*.mp3,*.pdf,*.tgz,*.flv,*.avi,*.mpeg,*.iso,*.epub' \
    $dump_domain
  fi
  unset dump_domain
}

### Entra a descargas

cdd () {
  if [ -d "$HOME/Descargas" ]; then
    cd $HOME/Descargas
  else
    cd $HOME/Downloads
  fi
}

### Entra a nubes

nube () {
  if [ -d "$HOME/Nube" ]; then
    cd $HOME/Nube
  else
    cd $HOME/Cloud
  fi
}

### Instalador de fuentes

font-add () {
  if [ ! -d "$FONT_DIR" ]; then
    mkdir $FONT_DIR
  fi
  cp $@ $FONT_DIR
  fc-cache -fv
}

### Obtiene mime type

mimetype () {
  file --mime-type $1 | cut -d " " -f 2
}

### Conversor de imágenes o documentos con ImageMagick o Pandoc

convertir () {
  case $1 in
    -h | --help | help | ayuda)
      echo "convertir INPUT_FILE OUTPUT_FILE" ;;
    *)
      echo "$1 => $2"
      if [[ $(mimetype $1) == *"video"* ]]; then
        ffmpeg -v quiet -stats -i $1 $2
      elif [[ $(mimetype $1) == *"image"* ]]; then
        convert -flatten $1 $2
      else
        doc2md $1 -o $2
      fi
      ;;
  esac
}

### Gestiona archivos MD

md () {
  format () {
    pandoc --reference-links --reference-location=document \
      --columns=$LENGTH -o $1 $1
  }

  case $1 in
  formatear | format | f)
    for arg in "${@:2}"; do
      if [ -d "$arg" ]; then
        for file in $arg/*.md; do format $file; done
      else
        format $arg
      fi
    done
    ;;
  convertir | convert)
    pandoc --standalone -V lang=es -o $4 $2 ;;
  leer | read | ver | cat | c)
    glow -w $LENGTH -p $2 ;;
  editar | edit | e)
    $EDITOR $2 ;;
  -h | --help | help | ayuda)
    md-help ;;
  esac
}

md-help () {
  echo "md: utilidad para hacer gestionar archivos Markdown.

Uso:
  md [OPT] MD

Opciones:
  formatear | format | f      Da formato a uno o más archivos.
  convertir | convert         Convierte el archivo.
  leer | read | ver | cat | c Muestra el archivo.
  editar | edit | e           Edita el archivo con $EDITOR.
  -h | --help | help | ayuda  Despliega esta ayuda.

Ejemplos:
  md formatear archivo.md
  md convertir archivo.md a archivo.html
  md leer archivo.md
  md editar archivo.md
  md ayuda"
}

### Obtiene directorio padre

parent () {
  if [ $# -eq 0 ]; then curr="$PWD"; else curr=$1; fi
  echo $(python3 -c """
from pathlib import Path
print(Path('$curr').resolve().parent)
  """)
  unset curr
}

### Desactiva o activa virtualenv

check-venv () {
  if [ $# -eq 0 ]; then curr="$PWD"; else curr=$1; fi
  if [ -f "$curr/$VENV/bin/activate" ]; then
    source $curr/$VENV/bin/activate
  elif [[ $(parent $1) != "/" ]]; then
    check-venv $(parent $1)
  else
    type deactivate &>/dev/null && deactivate
  fi
  unset curr
}

check-venv # Ejecuta cuando se carga

### Agrega y arranca virtualenv

venv () {
  virtualenv $VENV
  check-venv
}

### Modifica comportamiento de cd

cd () {
  builtin cd $1
  check-venv
}

### Modifica comportamiento de git

git () {
  if [ "$#" -lt 3 ] && [ "$1" = "push" ] && ( [ -z "$2" ] || [ "$2" = "--tags" ] ); then
    /usr/bin/git pushall
    if [ "$2" = "--tags" ]; then
      /usr/bin/git pushtags
    fi
  else
    /usr/bin/git $@
  fi
}

### Embellece y verifica código

pymp () {
  if [ ! -d "${@:$#}" ] && [ ! -f "${@:$#}" ]; then
    pympath=$(pwd)
  else
    pympath=${@:$#}
  fi
  case $1 in
  -f | formatear | format)
    ruff format --config $HOME/.config/ruff/config.toml ;;
  -h | --help | help | ayuda)
    pymp-help ;;
  esac
  ruff check --config $HOME/.config/ruff/config.toml
  unset pympath
}

pymp-help () {
  echo "pymp: utilidad para enchular código de Python.

Uso:
  pymp [OPT] [FILE|DIR]

Opciones:
  -f | formatear | format     Da formato a los archivos.
  -h | --help | help | ayuda  Despliega esta ayuda.

Ejemplos:
  pymp archivo.py
  pymp formatear .
  pymp ayuda"
  return 0
}

### Muesta contenido de archivo embellecido

c () {
  if [[ $1 == *.json ]]; then
    if [[ $(distro-name) = "Arch Linux" ]]; then
      cat $@ | jq | bat --file-name $1 --language json
    else
      cat $@ | jq | batcat --file-name $1 --language json
    fi
  else
    if [[ $(distro-name) = "Arch Linux" ]]; then
      bat $@
    else
      batcat $@
    fi
  fi
}

### Da formato a archivos

format () {
  if [[ $1 == *.md ]]; then
    md format $1
  elif [[ $1 == *.py ]]; then
    pymp format $1
  elif [[ $1 == *.lua ]]; then
    stylua $1
  elif [[ $1 == *.rs ]]; then
    rustfmt $1
  fi
}

### Compara archivos

compare () {
  if [[ $(md5sum "$1" | cut -d " " -f1) == $(md5sum "$2" | cut -d " " -f1) ]]
    then
      echo "true" && true
    else
      echo "false" && false
  fi
}

### Emula gitlab-runner sobre docker; cfr. https://stackoverflow.com/a/65920577

runner () {
  runner=gitlab-runner
  case $1 in
    -h | --help)
      runner-help ;;
    -i | --init)
      sudo docker run -d --name $runner -w $PWD -v $PWD:$PWD \
        -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner:latest 1>/dev/null
      ;;
    -rm | --remove)
      sudo docker rm -f $runner
        ;;
    *)
      sudo docker exec -it -e STAGE=$1 -w $PWD $runner \
        bash -c 'git config --global --add safe.directory "*" ; gitlab-runner exec docker $STAGE'
      ;;
  esac
  unset runner
}

runner-help () {
  echo "runner: utilidad para usar gitlab-runner en un contenedor.

Uso:
  runner [OPT]

Opciones:
  -h | --help    Despliega esta ayuda.
  -i | --init    Inicializa contenedor $runner en $PWD
  -rm | --remove Elimina contenedor $runner
  *              Stage de $runner a probar

Ejemplos:
  runner --init
  runner build
  runner test
  runner deploy
  runner -h
  runner -rm"
}

### Convierte STDOUT a TSV al reemplazar todos los códigos ANSI por tabuladores

totsv () {
  rgx_ansi_codes="\(\x1b\[[0-9;]*[mGKHF]\)\+"
  rgx_trim="s/^\s*//g;s/\s*$//g"
  if command -v gsed > /dev/null; then cmd=gsed; else cmd=sed; fi
  $cmd -u -e 's/\t/\\t/g' -e "s/$rgx_ansi_codes/\t/g" -e $rgx_trim
  unset cmd rgx_ansi_codes rgx_trim
}

### Genera lista de palabras únicas con número de concordancias

words () {
  cat $1 |
    grep -o -E '\w+' |
    tr '[:upper:]' '[:lower:]' |
    sort | uniq -c |
    sort -nr |
    awk '{$1=$1};1' |
    tr ' ' '\t'
}

### Divide dos PDF y los une para que queden alineados página por página

pdfparallelize () {
  size_a=$(qpdf --show-npages $1)
  size_b=$(qpdf --show-npages $2)
  if [ "$size_a" = "$size_b" ]; then
    dir=.pdfparallelize
    mkdir $dir
    qpdf --split-pages $1 $dir/page-%d-a.pdf
    qpdf --split-pages $2 $dir/page-%d-b.pdf
    qpdf --empty --pages $dir/*.pdf -- parallelized.pdf
    rm -rf $dir
    unset dir
  else
    echo "ERROR: Page count mismatch:\n$size_a\t$(basename $1)\n$size_b\t$(basename $2)"
  fi
  unset size_a size_b
}
